clc;
% theoretical result calculated by formula
thec_lambda = [125 150 175 200 225];
thec_mu = [244.14 244.14 244.14 244.14 244.14]
thec_throughput= thec_lambda./thec_mu;
thec_queue_length = thec_throughput.^2 ./(2*(1-thec_throughput));

 %%testbed result
 %lambda data for testbed experiment
 testbed_lambda_125 = [121.46 121.86 122.11 121.58 121.97]; % lambda is 125, repeat 5 times
 testbed_lambda_150 = [144.75 144.58 144.76 144.88 144.35];
 testbed_lambda_175 = [170.04 168.03 168.21 170.0 166.67];
 testbed_lambda_200 = [194.27 190.54 193.64 194.18 191.20];
 testbed_lambda_225 = [214.74 215.91 214.83 213.14 217.73];
 %mean lambda for testbed 
 mean_testbed_lambda = [mean(testbed_lambda_125), mean(testbed_lambda_150), mean(testbed_lambda_175), mean(testbed_lambda_200), mean(testbed_lambda_225)];
 
 %testbed throughput
 testbed_throughput = mean_testbed_lambda ./ thec_mu;
 %average queue length for testbed experiment
 qtestlam_125 = [0.0429907 0.0501859 0.0261682 0.0390335 0.0297398]; %for lambda is 125, repeat 5 times
 qtestlam_150 = [0.138318 0.0988806 0.132463 0.101313 0.143126];
 qtestlam_175 = [0.273921 0.266417 0.328972 0.344633 0.272556];
 qtestlam_200 = [0.981413 0.825603 1.31629 1.12336 1.05009];
 qtestlam_225 = [5.44693 4.3427 5.08922 6.50929 7.85366];
 mean_qtest = [mean(qtestlam_125), mean(qtestlam_150), mean(qtestlam_175), mean(qtestlam_200), mean(qtestlam_225)];
 std_qtest = [std(qtestlam_125), std(qtestlam_150), std(qtestlam_175), std(qtestlam_200), std(qtestlam_225)]
 
 
 %%ns2 Simulation result
 
 %lambda data for ns2 simulation
 actual_nslam_125 = [125.85 124.56 124.56 124.17 123.17]; % ideal lambda is 125, rep is 1-5, repeat 5 times
 actual_nslam_150 = [151.30 150.18 152.03 148.83 146.80];
 actual_nslam_175 = [175.38 174.45 177.30 175.15 170.25];
 actual_nslam_200 = [199.98 198.83 202.65 199.60 195.15];
 actual_nslam_225 = [224.77 222.95 228.83 224.48 219.45];
 % average lambda for ns2 simulation
 mean_actuallam = [mean(actual_nslam_125), mean(actual_nslam_150), mean(actual_nslam_175), mean(actual_nslam_200), mean(actual_nslam_225)];
 % ns2 simulation throughput
 ns_throughput = mean_actuallam ./thec_mu;
 
 %average queue length for ns2 simulation 
qnslam_125=[0.277628 0.275751 0.262114 0.274219 0.245126];%lambda is 125, and rep is 1-5
qnslam_150=[0.487161 0.517879 0.507209 0.498475 0.42827];
qnslam_175=[0.864974 0.941779 0.929488 0.922969 0.766306];
qnslam_200=[1.69299 1.8993 1.98951 1.85236 1.54988];
qnslam_225=[4.3649 5.0378 9.51784 4.8428 3.69164];
mean_qns = [mean(qnslam_125), mean(qnslam_150), mean(qnslam_175), mean(qnslam_200), mean(qnslam_225)];
std_qns = [std(qnslam_125), std(qnslam_150), std(qnslam_175), std(qnslam_200), std(qnslam_225)];
 
 %display data
 figure(1);
 clf;
 plot(thec_throughput,thec_queue_length,'-*r'); %plot theoretical result
 hold on;
 
 errorbar(thec_throughput,mean_qtest,std_qtest,'-ob'); %plot Testbed result
 title('Average Queue Length and ρ');
 hold on;
 
 errorbar(thec_throughput,mean_qns, std_qns,'-xk'); %plot simulation result
 xlabel('Throughput');
 ylabel('Average Queue Length (pkt)');
 title('Average Queue Length and Throughput for M/D/1 Queue');
 legend('Theoretical', 'Testbed','Simulation');
 
 figure(2);
 clf;
 plot(thec_throughput, thec_throughput, '-*r');
 hold on;
 plot(thec_throughput, testbed_throughput, '-ob');
 hold on;
 plot(thec_throughput, ns_throughput, '-xk');
 xlabel('Ideal Throughput');
 ylabel ('Realized Throughput');
 title('Ideal Throughput and Realized Throughput for M/D/1 Queue');
 legend('Theoretical', 'Testbed', 'Simulation');
 