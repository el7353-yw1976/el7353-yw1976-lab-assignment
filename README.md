#Background
I am going to measure the average queue length in M/D/1 Queue in the following scenario:
![background](http://i.imgur.com/RAwtVPn.jpg)
The λ and μ are the rate at which packets arrive at the queue (in packets/second) and the rate at which packets may be served by the queue (in packets/second).  
ρ is the utilization factor, which is λ/μ.
We define E{X} = 1/μ is the average service time, var{X} =0.
According to the P-K formular:
The average time delay in the system is: E{T} = (1-ρ/2)[(1/μ)/(1-ρ)]

The average time delay in the queue is: E{T} - 1/μ = ρ/[2μ(1-ρ)]

According to the little's Formular: 
The mean number in the queue is: N(Q) = λ*E{T} = ρ^2/[2(1-ρ)] 

From the equation, the relationship between ρ and N(Q) is the following figure:
![analytical queue length](http://i.imgur.com/ZlhLuPV.jpg)
#Result
The result of testbed experiment and ns2 simulation is the figure in the following:
![result queue](http://imgur.com/fusTf6z.jpg)

From the figure, we can see that the trend of the average queue length in testbed experiment and ns2 simulation are same. However, the ns2 simulation more closely mimics the theoretical result, because it is essentially just an implementation of the model.  The testbed experiment is more realistic and may potentially expose behaviors of real queues that are not captured by the model. 
The story that the figure tries to tell is that the relationship between average queue length and utilization factor in M/D/1 satisfy the equation N(Q) = λ*E{T} = ρ^2/[2(1-ρ)], therefore the assumption is correct.

Besides, the realized throughput in testbed and simulation against ideal throughput is the figure in the following:
![Throughput](http://imgur.com/ufNmMpH.jpg)
#Run the experiment
we're going to experimentally measure the queue length of an M/D/1 queue for a range of utilization factors. For convenience, I will keep everything constant and only vary λ from one experiment to the next.  For every factor level, we run the same experiment 5 times.
##Testbed experiment
Set up the queue on my router to pass traffic at a rate of 1 Mbps by using the following command:
`sudo tc qdisc replace dev eth2 root tbf rate 1mbit limit 200mb burst 32kB peakrate 1.01mbit mtu 1600`
Then, I will run ITGSend with a packet size of 512 B, but varying packet arrival rate.  I should run 5 experiments for the following values of λ: 225, 200, 175, 150, 125.

Before the exoeriment, I need to install D-ITG on client and server node. log in to each and run
`sudo apt-get install d-itg`

The steps is the following:

1: Record the requested λ in my experiment log.

2: Start `ITGRecv` on the server node.

3: Start ITGSend with the following command on the client node.
`ITGSend -a server -l sender.log -x receiver.log -E 225 -c 512 -T UDP -t 85000`
For next experiment, I should change -E 225 to different λ value.  The duration is 85s. 

4:	After about five seconds, start the queue monitor script on the router node by typing the command:
`./queuemonitor.sh eth2 60 0.1 | tee router.txt`
The time that the queuemonitor.sh runs is 60s.

5:	When ITGSend ends, use `ITGDec receiver.log` and `ITGDec sender.log`"on the server node and client node to decode the log. Find the actual packet arrival rate (λ) and the actual average packet size (μ is equal to the queue rate, divided by the average packet size in bytes.) ,calculate the realized value of ρ and then calculate the theoretical average queue size for this value of ρ. 

6:  For every factor level, I get 5 experiment result and then comlute the average value.  To plot the figure, I use MATLAB. The script `trendplot.m` and `queueplot.m`are attached.

##ns2 simulation 
The `md1.tcl` script is attached.
The step is:

1:  Run experiments for the following values of lambda: 225, 200, 175, 150, 125.  rep is 1, 2, 3, 4, 5 for every factor level.  When you run the script, please input two arguments.  The first one is the rep from 1 to 5, and the second one is lambda. 

2: I pick up server nose to do the ns2 simulation. run `sudo apt-get install ns2` at server node. 
run the command `ns md1.tcl 1 125` on server node. For different rep, change the argument. 

3: According to qm.out, run the command `cat qm.out | awk  '{ sum += $5 } END { if (NR > 0) print sum / NR }' `find the average queue length.  From the qm.out, also record the values of λ and μ.
4: Calculate ρ and responding average queue length and then plot it together with analytical result and testbed experimental result using MATLAB. 